#!/bin/bash


LAST_COMMIT=$(git ls-remote git://github.com/cz-nic/knot-resolver.git refs/heads/master | cut -f 1)
SDK_NAME=OpenWrt-SDK-mvebu_gcc-4.8-linaro_musl-1.1.15_eabi.Linux-x86_64
SDK_URL=https://repo.turris.cz/omnia
BUILD_DIR=$(pwd)
SDK_DIR=$BUILD_DIR/turris/$SDK_NAME

echo "Download sdk" && date +"%T.%N"

#set SDK
wget --no-check-certificate $SDK_URL/$SDK_NAME.tar.bz2 --directory-prefix=/tmp
mkdir -p $BUILD_DIR/turris/


echo "Extract sdk" && date +"%T.%N"
tar xjf /tmp/$SDK_NAME.tar.bz2 --directory=$BUILD_DIR/turris
rm -rf /tmp/$SDK_NAME.tar.bz2


echo "Clone template" && date +"%T.%N"
#edit template openwrt package
git clone --depth 1 https://github.com/ja-pa/knot-resolver-turris.git
sed  -i "s/COMMIT_HASH/$LAST_COMMIT/" knot-resolver-turris/knot-resolver/Makefile


#copy template
cp -r knot-resolver-turris/knot-resolver $SDK_DIR/package/


echo "Build package" && date +"%T.%N"
#build package
cd $SDK_DIR
export PATH=$PATH:$BUILD_DIR/turris/$SDK_NAME/staging_dir/toolchain-arm_cortex-a9+vfpv3_gcc-4.8-linaro_musl-1.1.15_eabi/bin
USE_CCACHE=n make CC=arm-openwrt-linux-gcc CXX=arm-openwrt-linux-g++ LD=arm-openwrt-linux-ld -C $SDK_DIR V=s

